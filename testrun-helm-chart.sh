#!/bin/bash

set -ex


function f-wait-for-pod-run() {
    # wait for pod Running
    local POD_NAME=$1
    local count=0
    while true
    do
        sleep 2
        local STATUS=$(kubectl get pod/${POD_NAME} | awk '{print $3}' | grep Running)
        RC=$? ; if [ $RC -ne 0 ]; then echo "error. abort." ; return $RC; fi
        if [ ! -z "$STATUS" ]; then
            break
        fi
        echo -n -e "waiting for running pod ... $count seconds \r"
        sleep 3
        count=$( expr $count + 5 )
        if [ $count -gt 300 ]; then
            echo "timeout for pod Running. abort."
            return 1
        fi
    done
}


# convert mysquid.yaml(docker-compose.yml) to helm chart
/bin/rm -rf mysquid
kompose convert -f mysquid.yaml --chart  --controller deployment

# helm install
helm install mysquid  --name mysquid  
# wait for install
kubectl rollout status deploy/mysquid

# start testing pod
POD_NAME=test-mysquid-test

# run docker image
export http_proxy=mysquid:3128
export https_proxy=mysquid:3128
kubectl run  ${POD_NAME} \
    --restart=Never \
    --image=georgesan/mycentos7docker:latest   \
    --env="http_proxy=${http_proxy}" --env="https_proxy=${https_proxy}" \
    --command -- sleep 3600

# wait for pod Running
f-wait-for-pod-run ${POD_NAME}

# execute curl command via squid
kubectl exec ${POD_NAME} \
    --  bash -c " env ; curl -v https://www.yahoo.co.jp/ "

# delete pod
kubectl delete pod ${POD_NAME}

# delete helm
helm delete --purge  mysquid

