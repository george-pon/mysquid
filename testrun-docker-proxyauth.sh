#!/bin/bash

set -ex

docker run --name mysquid -d -p 3128:3128 -e PROXY_USER_PASS_LIST="cent:pass scott:tiger" mysquid:latest
sleep 5

docker exec mysquid bash -c "ss -antup"
sleep 5

# success
export http_proxy=http://cent:pass@192.168.33.10:3128/
export https_proxy=http://cent:pass@192.168.33.10:3128/
export no_proxy=127.0.0.1,localhost,192.168.33.10,192.168.33.11
if curl -v https://www.yahoo.co.jp/  ; then
    echo SUCCESS
else
    echo FAILURE
fi


# no pass
export http_proxy=http://192.168.33.10:3128/
export https_proxy=http://192.168.33.10:3128/
export no_proxy=127.0.0.1,localhost,192.168.33.10,192.168.33.11
if curl -v https://www.yahoo.co.jp/  ; then
    echo FAILURE
else
    echo SUCCESS
fi

# wrong pass
export http_proxy=http://cent:wrong@192.168.33.10:3128/
export https_proxy=http://cent:wrong@192.168.33.10:3128/
export no_proxy=127.0.0.1,localhost,192.168.33.10,192.168.33.11
if curl -v https://www.yahoo.co.jp/  ; then
    echo FAILURE
else
    echo SUCCESS
fi

export http_proxy=
export https_proxy=
export no_proxy=

docker logs mysquid
sleep 5

docker rm -f mysquid
