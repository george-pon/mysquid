# mysquid

squid container based on CentOS 7

### how to use

```
# normal squid proxy
docker run -d -p 3128:3128 george-san/mysquid:latest

# squid proxy with basic authentication
docker run -d -p 3128:3128 -e PROXY_USER_PASS_LIST="user1:pass1 user2:pass2 scott:tiger" george-san/mysquid:latest

# squid proxy with upper authorized proxy
docker run -d -p 3128:3128 -e UPPER_PROXY_PEER_1="cache_peer main.proxy.com parent 8080 0 no-query no-netdb-exchange no-digest login=proxyuser:proxypassword default" george-san/mysquid:latest

```

### environment variables

* PROXY_USER_PASS_LIST    If set , squid requires basic authentication. Set username:password list like docker run -e PROXY_USER_PASS_LIST="user1:pass1 user2:pass2 scott:tiger" .
    example: PROXY_USER_PASS_LIST="user1:pass1 user2:pass2 scott:tiger"

* UPPER_PROXY_PEER_1      If set , add cache_peer line into squid.conf
    example: UPPER_PROXY_PEER_1="cache_peer main.proxy.com parent 8080 0 no-query no-netdb-exchange no-digest login=proxyuser:proxypassword default"

* PROXY_ACL_LOCALNET      If set , add acl localnet src line into squid.conf
    example: PROXY_ACL_LOCALNET="192.168.0.0/16"

