#!/bin/bash

set -ex

docker run --name mysquid -d -p 3128:3128 mysquid:latest
sleep 5

docker exec mysquid bash -c "ss -antup"
sleep 5

export http_proxy=192.168.33.10:3128
export https_proxy=192.168.33.10:3128
if curl -v https://www.yahoo.co.jp/  ; then
    echo SUCCESS
else
    echo FAILURE
fi

docker exec mysquid bash -c "ss -antup"
sleep 5

docker rm -f mysquid
