#!/bin/bash

MODE=
if [ x"$1"x = x"run"x ]; then
    MODE=run
fi

set -ex

docker-compose -f mysquid.yaml  up  -d

export http_proxy=127.0.0.1:3128
export https_proxy=127.0.0.1:3128
if curl -v https://www.yahoo.co.jp/  ; then
    echo SUCCESS
else
    echo FAILURE
fi

if [ -z "$MODE" ]; then
    docker-compose -f mysquid.yaml down
fi
