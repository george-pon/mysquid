#!/bin/bash

set -x

while [ $# -gt 0 ]
do
    if [ x"$1"x = x"-e"x ]; then
        eval $2
        export ${2%%=*}
        mkdir -p /etc/profile.d
        echo "export $2" >> /etc/profile.d/docker-entrypoint-env.sh
        shift
        shift
        continue
    fi
    break
done

if [ -r /etc/profile.d/docker-entrypoint-env.sh ]; then
    .  /etc/profile.d/docker-entrypoint-env.sh
fi

# set proxy password  user1:pass1 user2:pass2 ...:...
for i in $PROXY_USER_PASS_LIST
do
    j=$( echo $i | sed -e 's/:/ /g' )
    if [ -r /etc/squid/.htpasswd ] ; then
        htpasswd -b    /etc/squid/.htpasswd $j
    else
        htpasswd -b -c /etc/squid/.htpasswd $j
    fi
done

#
# add acl localnet src
#
#  PROXY_ACL_LOCALNET="192.168.0.0/16  172.16.0.0/12"
#
for i in $PROXY_ACL_LOCALNET
do
    sed -i -e 's%# ADD_LOCALNET%acl localnet src '$i'%g'  /etc/squid/squid.conf
    sed -i -e 's%# ADD_LOCALNET%acl localnet src '$i'%g'  /etc/squid/squid-proxyauth.conf
done


#
# set upper proxy settings
#
# UPPER_PROXY_PEER_1="cache_peer main.proxy.com parent 8080 0 no-query no-netdb-exchange no-digest login=proxyuser:proxypassword default"
#
for i in "$UPPER_PROXY_PEER_1" "$UPPER_PROXY_PEER_2"
do
    if [ -z "$i" ]; then
        continue
    fi
    echo "" >> /etc/squid/squid.conf
    echo "" >> /etc/squid/squid-proxyauth.conf
    echo "$i" >> /etc/squid/squid.conf
    echo "$i" >> /etc/squid/squid-proxyauth.conf
    echo "never_direct allow all" >> /etc/squid/squid.conf
    echo "never_direct allow all" >> /etc/squid/squid-proxyauth.conf
    echo "never_direct allow CONNECT" >> /etc/squid/squid.conf
    echo "never_direct allow CONNECT" >> /etc/squid/squid-proxyauth.conf
done

#
# run squid daemon
#
# disable ICP port (-u 0)
#
if [ -z "$PROXY_USER_PASS_LIST" ]; then
    /usr/sbin/squid -f /etc/squid/squid.conf -u 0
else
    /usr/sbin/squid -f /etc/squid/squid-proxyauth.conf -u 0
fi

tail -F /var/log/squid/cache.log  /var/log/squid/access.log
