FROM centos:centos7

ENV MYSQUID_VERSION build-target
ENV MYSQUID_VERSION latest
ENV MYSQUID_VERSION stable
ENV MYSQUID_IMAGE mysquid

# install CentOS Project GPG public key
RUN rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7

# install network utils
RUN yum install -y iproute net-tools bind-utils && yum clean all

# install squid and htpasswd
RUN yum install -y squid httpd-tools && yum clean all

# add squid conf files
ADD squid.conf /etc/squid/squid.conf
ADD squid-proxyauth.conf /etc/squid/squid-proxyauth.conf

# add docker-entrypoint.sh
ADD docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh
RUN chmod +x /usr/local/bin/docker-entrypoint.sh

CMD ["/usr/local/bin/docker-entrypoint.sh"]

EXPOSE 3128
